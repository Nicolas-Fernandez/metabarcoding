###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: perform diversity metrics and rarefaction
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.16
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

# Params
METADATA = config["datasets"]["metadata"] # general

JOBS = config["cluster"]["threads"]       # metrics

################################################################################
def get_rarefaction_depth(wildcards):
    splited = wildcards.split("_")
    project = splited[0]
    primers = splited[1]
    return  config["depth"][project][primers]

################################################################################
rule all:
    input:
        rarefied_table  = expand("out/{project}/{primers}/qiime2/visual/"
                                 +"RarTable.qzv",
                                 project = PROJECT,
                                 primers = PRIMERS),
        
        rarefied_repseq = expand("out/{project}/{primers}/qiime2/visual/"
                                 +"RarRepSeq.qzv",
                                 project = PROJECT,
                                 primers = PRIMERS)

################################################################################
rule summarize_table:
    # Aim: Generate visual and tabular summaries of a feature table
    # Use: qiime feature-table summarize [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        visualization = "out/{project}/{primers}/qiime2/visual/RarTable.qzv"
    shell:  
        "qiime feature-table summarize "
        "--i-table {input.rarefied_table} "
        "--m-sample-metadata-file {params.metadata} "
        "--o-visualization {output.visualization}"

################################################################################
rule summarize_sequence:
    # Aim: Generate tabular view of feature identifier to sequence mapping
    # Use: qiime feature-table tabulate-seqs [OPTIONS]
    conda:
        QIIME2
    input:
        rarefied_repseq = "out/{project}/{primers}/qiime2/core/RarRepSeq.qza"
    output:
        visualization = "out/{project}/{primers}/qiime2/visual/RarRepSeq.qzv"
    shell:
        "qiime feature-table tabulate-seqs "
        "--i-data {input.rarefied_repseq} "
        "--o-visualization {output.visualization}"

################################################################################
rule sequence_rarefaction_filter:
    # Aim: Filter features from sequences based on a feature table or metadata.
    # Use: qiime feature-table filter-seqs [OPTIONS]
    conda:
        QIIME2
    input:
        filtered_repseq = "out/{project}/{primers}/qiime2/core/ConRepSeq.qza",
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        rarefied_repseq = "out/{project}/{primers}/qiime2/core/RarRepSeq.qza"
    shell:
        "qiime feature-table filter-seqs "
        "--i-data {input.filtered_repseq} "
        "--i-table {input.rarefied_table} "
        "--o-filtered-data {output.rarefied_repseq}"

################################################################################
rule core_metrics_phylogenetic:
    # Aim: Applies a collection of diversity metrics to a feature table
    # Use: qiime diversity core-metrics-phylogenetic [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA,
        jobs = JOBS,
        depth = lambda wildcards: get_rarefaction_depth(str(wildcards.project
                                                            +"_"
                                                            +wildcards.primers))
    input:
        rooted_tree = "out/{project}/{primers}/qiime2/tree/RootedTree.qza",
        filtered_table = "out/{project}/{primers}/qiime2/core/ConTable.qza"
    output:
        # rarefied feature table
        rarefied_table = ("out/{project}/{primers}/qiime2/core/"
                         +"RarTable.qza"),
        # observed_otus, qualitative measure of community richness
        observed_asv = ("out/{project}/{primers}/qiime2/core/"
                       +"Vector-observed_asv.qza"),
        # shannon (H), quantitative measure of community richness
        shannon = ("out/{project}/{primers}/qiime2/core/"
                   +"Vector-shannon.qza"),
        # pielou_e (J), qualitative measure of community evenness
        evenness = ("out/{project}/{primers}/qiime2/core/"
                    +"Vector-evenness.qza"),
        # faith_pd, qualitative measure of community richness,
         # with phylogenetic relationships between features
        faith_pd = ("out/{project}/{primers}/qiime2/core/"
                   +"Vector-faith_pd.qza"),
        # jaccard, qualitative measure of community dissimilarity
        jaccard = ("out/{project}/{primers}/qiime2/core/"
                   +"Matrix-jaccard.qza"),
        # braycurtis, quantitative measure of community dissimilarity
        braycurtis = ("out/{project}/{primers}/qiime2/core/"
                      +"Matrix-braycurtis.qza"),
        # unweighted_unifrac, qualitative measure of community dissimilarity,
         # with phylogenetic relationships between features
        unweighted = ("out/{project}/{primers}/qiime2/core/"
                      +"Matrix-unweighted_unifrac.qza"),
        # weighted_unifrac, quantitative measure of community dissimilarity,
         # with phylogenetic relationships between features
        weighted = ("out/{project}/{primers}/qiime2/core/"
                    +"Matrix-weighted_unifrac.qza"),
        # PCoA
        pcoa_jaccard = ("out/{project}/{primers}/qiime2/pcoa/"
                       +"PCoA-jaccard.qza"),
        pcoa_braycurtis = ("out/{project}/{primers}/qiime2/pcoa/"
                          +"PCoA-braycurtis.qza"),
        pcoa_unweighted = ("out/{project}/{primers}/qiime2/pcoa/"
                          +"PCoA-unweighted_unifrac.qza"),
        pcoa_weighted = ("out/{project}/{primers}/qiime2/pcoa/"
                        +"PCoA-weighted_unifrac.qza"),
        # Emperor
        emperor_jaccard = ("out/{project}/{primers}/qiime2/visual/"
                          +"Emperor-jaccard.qzv"),
        emperor_braycurtis = ("out/{project}/{primers}/qiime2/visual/"
                             +"Emperor-braycurtis.qzv"),
        emperor_unweighted = ("out/{project}/{primers}/qiime2/visual/"
                             +"Emperor-unweighted_unifrac.qzv"),
        emperor_weighted = ("out/{project}/{primers}/qiime2/visual/"
                           +"Emperor-weighted_unifrac.qzv")
    shell:
        "qiime diversity core-metrics-phylogenetic "
        "--i-phylogeny {input.rooted_tree} "
        "--i-table {input.filtered_table} "
        #
        "--p-sampling-depth {params.depth} "
        "--p-n-jobs {params.jobs} "
        "--m-metadata-file {params.metadata} "
        #
        "--o-rarefied-table {output.rarefied_table} "
        #
        "--o-observed-otus-vector {output.observed_asv} "
        "--o-shannon-vector {output.shannon} "
        "--o-evenness-vector {output.evenness} "
        "--o-faith-pd-vector {output.faith_pd} "
        #
        "--o-jaccard-distance-matrix {output.jaccard} "
        "--o-bray-curtis-distance-matrix {output.braycurtis} "
        "--o-unweighted-unifrac-distance-matrix {output.unweighted} "
        "--o-weighted-unifrac-distance-matrix {output.weighted} "        
        #
        "--o-jaccard-pcoa-results {output.pcoa_jaccard} "
        "--o-bray-curtis-pcoa-results {output.pcoa_braycurtis} "
        "--o-unweighted-unifrac-pcoa-results {output.pcoa_unweighted} "
        "--o-weighted-unifrac-pcoa-results {output.pcoa_weighted} "
        #
        "--o-jaccard-emperor {output.emperor_jaccard} "        
        "--o-bray-curtis-emperor {output.emperor_braycurtis} "
        "--o-unweighted-unifrac-emperor {output.emperor_unweighted} "
        "--o-weighted-unifrac-emperor {output.emperor_weighted}"

################################################################################
