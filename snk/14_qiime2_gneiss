###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: identify features that are differentially abundant across groups
# Date: 2018.11.20
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.20
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

# Params
METADATA = config["datasets"]["metadata"] # general

GRADIENT = config["gneiss"]["gradient"] # gradient-clustering

FORMULA = config["gneiss"]["formula"] # ols_regression

N_DIMENSION = config["gneiss"]["heatmap"]["n_dimension"] # dendrogram_heatmap
METHOD = config["gneiss"]["heatmap"]["method"]           # dendrogram_heatmap
COLOR_MAP = config["gneiss"]["heatmap"]["color_map"]     # dendrogram_heatmap
FACTOR = config["gneiss"]["heatmap"]["factor"]           # dendrogram_heatmap

LEVEL = config["gneiss"]["balance"]["level"] # balance_taxonomy
 
###############################################################################
rule all:
    input: 
        taxa_summary = expand("out/{project}/{primers}/qiime2/visual/"
                              +"y0TaxaSum.qzv",
                              project = PROJECT,
                              primers = PRIMERS)

###############################################################################
rule balance_taxonomy:
    # Aim: Visualize the distribution of a single balance and summarize
     # its numerator and denominator components
    # Use: qiime gneiss balance-taxonomy [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA,
        level = LEVEL,
        factor = FACTOR
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza",
        correlation_hierarchy = ("out/{project}/{primers}/qiime2/gneiss/"
                              +"CorrHierarchy.qza"),
        taxonomy = "out/{project}/{primers}/qiime2/taxonomy/Taxonomy.qza"
    output:
        taxa_summary = "out/{project}/{primers}/qiime2/visual/y0TaxaSum.qzv"
    shell:
        "qiime gneiss balance-taxonomy "
        "--i-table {input.rarefied_table} "
        "--i-tree {input.correlation_hierarchy} "
        "--i-taxonomy {input.taxonomy} "
        "--p-taxa-level {params.level} "
        "--p-balance-name 'y0' "
        "--m-metadata-file {params.metadata} "
        "--m-metadata-column {params.factor} "
        "--o-visualization {output.taxa_summary}"

###############################################################################
rule dendrogram_heatmap:
    # Aim: Visualize the feature table as a heatmap
    # Use: qiime gneiss dendrogram-heatmap [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA,
        n_dimension = N_DIMENSION,
        method = METHOD,
        color_map = COLOR_MAP,
        factor = FACTOR
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza",
        correlation_hierarchy = ("out/{project}/{primers}/qiime2/gneiss/"
                              +"CorrHierarchy.qza")
    output:
        regression_summary = "out/{project}/{primers}/qiime2/visual/RegSum.qzv"
    shell:
        "qiime gneiss dendrogram-heatmap "
        "--i-table {input.rarefied_table} "
        "--i-tree {input.correlation_hierarchy} "
        "--m-metadata-file {params.metadata} "
        "--m-metadata-column {params.factor} "
        "--p-color-map {params.color_map} "
        "--p-method {params.method} "
        "--p-ndim {params.n_dimension} "
        "--o-visualization {output.regression_summary}"

###############################################################################
rule ols_regression:
    # Aim: Perform linear regression on balances
    # Use: qiime gneiss ols-regression [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA,
        formula = FORMULA
    input:
        balance = "out/{project}/{primers}/qiime2/gneiss/Balance.qza",
        correlation_hierarchy = ("out/{project}/{primers}/qiime2/gneiss/"
                              +"CorrHierarchy.qza")
    output:
        regression_summary = "out/{project}/{primers}/qiime2/visual/Regression"
    shell:
        "qiime gneiss ols-regression "
        "--p-formula '{params.formula}' "
        "--i-table {input.balance} "
        "--i-tree {input.correlation_hierarchy} "
        "--m-metadata-file {params.metadata} "
        "--o-visualization {output.regression_summary}"

###############################################################################
rule isometric_log_ratio:
    # Aim: Calculate balances given a hierarchy
    # Use: qiime gneiss ilr-hierarchical [OPTIONS]
    conda:
        QIIME2
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza",
        correlation_hierarchy = ("out/{project}/{primers}/qiime2/gneiss/"
                              +"CorrHierarchy.qza")
    output:
        balance = "out/{project}/{primers}/qiime2/gneiss/Balance.qza"
    shell:
        "qiime gneiss ilr-hierarchical "
        "--i-table {input.rarefied_table} "
        "--i-tree {input.correlation_hierarchy} "
        "--o-balances {output.balance}"

###############################################################################
rule gradient_clustering:
    # Aim: Build bifurcating tree representing features hierarchical clustering
    # Use: qiime gneiss gradient-clustering [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA,
        gradient = GRADIENT
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        gradient_hierarchy = ("out/{project}/{primers}/qiime2/gneiss/"
                              +"GradHierarchy.qza")
    shell:
        "qiime gneiss gradient-clustering "
        "--i-table {input.rarefied_table} "
        "--m-gradient-file {params.metadata} "
        "--m-gradient-column {params.gradient} "
        "--o-clustering {output.gradient_hierarchy}"

"""
Notes:
An important consideration for downstream analyses is  overfitting.
When using gradient-clustering, you are creating a tree to best highlight
compositional differences along the metadata category of your choice,
and it's possible to get false positives. Use gradient-clustering with caution.
"""

###############################################################################
rule correlation_clustering:
    # Aim: Build bifurcating tree representing features hierarchical clustering
    # Use: qiime gneiss correlation-clustering [OPTIONS]
    conda:
        QIIME2
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza"
    output:
        correlation_hierarchy = ("out/{project}/{primers}/qiime2/gneiss/"
                                 +"CorrHierarchy.qza")
    shell:
        "qiime gneiss correlation-clustering "
        "--i-table {input.rarefied_table} "
        "--o-clustering {output.correlation_hierarchy}"

###############################################################################
