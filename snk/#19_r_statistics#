####################
configfile: 'config.yaml'

####################
from snakemake.utils import R

####################
def get_order(wildcards):
    return config['r']['graph']['order'][wildcards]

def get_color(wildcards):
     return config['r']['graph']['color'][wildcards]

def get_shape(wildcards):
     return config['r']['graph']['shape'][wildcards]

def get_names(wildcards):
    return config['r']['graph']['names'][wildcards]

####################/
rule all:
    input:
        Plots = expand('out/{data}/{primer}/r/alpha_diversity/{filt}/{alpha}/barplot{letter}.{ext}',
                       data = config['datasets']['data'],
                       primer = config['datasets']['primer'],
                       filt = config['datasets']['filter']['rare'],
                       alpha = config['diversity']['alpha']['significance'],
                       letter = ['S','M'],
                       ext = config['r']['extension']),
        
        BetaNMDS = expand('out/{data}/{primer}/r/nmds/{filt}/{beta}.{ext}',
                          data = config['datasets']['data'],
                          primer = config['datasets']['primer'],
                          filt = config['datasets']['filter']['rare'],
                          beta = config['diversity']['beta']['significance'],
                          ext = config['r']['extension']),
        
        AbundPCA = expand('out/{data}/{primer}/r/pca/{filt}/Abundance.{ext}',
                          data = config['datasets']['data'],
                          primer = config['datasets']['primer'],
                          filt = config['datasets']['filter']['rare'],
                          ext = config['r']['extension']),

        Frequency = expand('out/{data}/{primer}/r/frequency/{filt}/frequency-species.{ext}',
                           data = config['datasets']['data'],
                           primer = config['datasets']['primer'],
                           filt = config['datasets']['filter']['rare'],
                           ext = config['r']['extension']),

        Rank = expand('out/{data}/{primer}/r/rank/{filt}/rank-abundance.done',
                      data = config['datasets']['data'],
                      primer = config['datasets']['primer'],
                      filt = config['datasets']['filter']['rare']),
        
        Adonis = expand('out/{data}/{primer}/r/adonis/{filt}/adonis-{category}-PermANOVA-{methode}.txt',
                        data = config['datasets']['data'],
                        primer = config['datasets']['primer'],
                        filt = config['datasets']['filter']['rare'],
                        category = config['permanova']['category'],
                        methode = config['r']['dissimethode'])
        
        #Significance = expand('out/{data}/{primer}/r/adonis/{filt}/Phylum-Significance.txt',
        #                      #data = config['datasets']['data'],
        #                      data = 'SUBPLOT',
        #                      primer = config['datasets']['primer'],
        #                      #filt = config['datasets']['filter']['rare'])
        #                      filt = 'RareFiltCont')


####################
rule Alpha_Diversity:
    #Aim: Draw alpha diversity index boxplots.
    #Use: "ggplot2" R package
    log:
        'out/{data}/{primer}/r/alpha_diversity/{filt,.*}/{alpha}/{alpha}.log'
    params:
        mirror = config['r']['mirror'],
        order = lambda wildcards: get_order(wildcards.data),
        color = lambda wildcards: get_color(wildcards.data)
    input:
        Table = 'out/{data}/{primer}/qiime2/export/core/{filt,.*}-vector-{alpha}/alpha-diversity.tsv'
    output:
        BarplotS = 'out/{data}/{primer}/r/alpha_diversity/{filt,.*}/{alpha}/barplotS.{ext}',
        BarplotM = 'out/{data}/{primer}/r/alpha_diversity/{filt,.*}/{alpha}/barplotM.{ext}',
        Boxplot = 'out/{data}/{primer}/r/alpha_diversity/{filt,.*}/{alpha}/boxplot.{ext}',
        Homogenity = 'out/{data}/{primer}/r/alpha_diversity/{filt,.*}/{alpha}/homogenity.{ext}',
        Normality = 'out/{data}/{primer}/r/alpha_diversity/{filt,.*}/{alpha}/normality.{ext}'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("ggplot2")) {{install.packages("ggplot2", repos="{params.mirror}")}}
            if (!require("ggthemes")) {{install.packages("ggthemes", repos="{params.mirror}")}}
            if (!require("RColorBrewer")) {{install.packages("RColorBrewer", repos="{params.mirror}")}}
            if (!require("gridExtra")) {{install.packages("gridExtra", repos="{params.mirror}")}}
            if (!require("dplyr")) {{install.packages("dplyr", repos="{params.mirror}")}}
            if (!require("ggpubr")) {{install.packages("ggpubr", repos="{params.mirror}")}}
            if (!require("car")) {{install.packages("car", repos="{params.mirror}")}}
            if (!require("lmPerm")) {{install.packages("lmPerm", repos="{params.mirror}")}}
            if (!require("dunn.test")) {{install.packages("dunn.test", repos="{params.mirror}")}}
            if (!require("multcompView")) {{install.packages("multcompView", repos="{params.mirror}")}}
            #
            ### LOAD LIBRARIES
            library(ggplot2)      # You provide the data, tell 'ggplot2' how to map variables to aesthetics, what graphical primitives to use, and it takes care of the details.
            library(ggthemes)     # This package contains extra themes, scales, and geoms, and functions for and related to 'ggplot2'.
            library(RColorBrewer) # Creates nice looking color palettes especially for thematic maps. Use: display.brewer.all()
            library(grid)         # 'grid' adds an 'nx' by 'ny' rectangular grid to an existing plot.
            library(gridExtra)    # Provides a number of user-level functions to work with 'grid' graphics, notably to arrange multiple grid-based plots on a page, and draw tables.
            library(dplyr)        # dplyr provides a flexible grammar of data manipulation. It's the next iteration of plyr, focused on tools for working with data frames (hence the _d_ in the name).
            library(ggpubr)       #
            library(car)          # Functions and Datasets to Accompany J. Fox and S. Weisberg, An R Companion to Applied Regression, Second Edition, Sage, 2011.
            library(lmPerm)       # 'aovp' is 'aov' modified to use permutation tests instead of normal theory tests. Like 'aov', the ANOVA model is fitted by a call to 'lmp' for each stratum.
            library(dunn.test)    # Performs Dunn's test of multiple comparisons using rank sums and reports the results among multiple pairwise comparisons after a Kruskal-Wallis.
            library(multcompView) #
            #
            ### IMPORT ALPHA DIVERSITY INDEX
            DATA<-read.delim("{input.Table}", dec=".")                                       # Import table as table
            rownames(DATA) <- DATA[,1]                                                       # Fix rownames as column 1
            colnames(DATA)[1] <- "samples"                                                   # rename column 1 as "samples"
            DATA$formations <- sapply(strsplit(as.character(row.names(DATA)), "_"), "[[", 1) # Create a column of formations names, from the rownames of DATA
            DATA$formations <- as.factor(DATA$formations)                                    # formations as factor
            DATA$formations <- ordered(DATA$formations, levels = c({params.order}))          # re-order levels (mod) in the wanted order
            #
            ### OPEN SINK
            sink("{log}")
            #
            ### CALCUL MEAN SD BY ...
            FORM <- group_by(DATA, formations) %>% summarise(count = n(), mean = mean({wildcards.alpha}, na.rm = TRUE), sd = sd({wildcards.alpha}, na.rm = TRUE)) # Calcul mean/sd by formation
            print("")
            print("")
            print("___________________")
            print("### MEAN and SD ###")
            print("-------------------")
            print("")
            print(FORM)
            #
            ### ONE-WAY ANOVA (parametric) --> H0 : sample follow same normal law (mean, variance, ...)
            RES.AOV <- aov({wildcards.alpha} ~ formations, data = DATA) # Compute the analysis of variance
            print("")
            print("")
            print("__________________________________")
            print("### ONE-WAY ANOVA (parametric) ###")
            print("----------------------------------")
            print("")
            print(summary(RES.AOV)) # Summary of the analysis
            #
            ### POST-HOC (parametric) TESTS / Multiple Comparison (parametric) Test
            print("")
            print("")
            print("____________________________")
            print("### TUKEY HSD (post-hoc) ###")
            print("----------------------------------")
            print("")
            print(TukeyHSD(RES.AOV)) # Tukey HSD (honest significant difference) test
            print("")
            print("")
            print("__________________________________")
            print("### PAIRWISE T TEST (post-hoc) ###")
            print("----------------------------------")
            print("")
            print(pairwise.t.test(DATA${wildcards.alpha}, DATA$formations, p.adjust.method = "BH"))
            # Pairewise t-test with corrections (BH) for multiple testing
            #
            ### VARIANCES HOMOGENEITY ??? Levene's test --> H0 : group (formations) variances are equal
            {wildcards.ext}("{output.Homogenity}")  # Save plot save as .pdf and .png
            plot(RES.AOV, 1) # Plot residual
            dev.off()        # dev.off
            print("")
            print("")
            print("_________________________________")
            print("### LEVENE TEST (homogeneity) ###")
            print("---------------------------------")
            print("")
            print(leveneTest({wildcards.alpha} ~ formations, data = DATA)) # Levene's test
            print("")
            print("")
            print("_____________________________________________________________")
            print("### ONE-WAY ANOVA (without assumption of equal variances) ###")
            print("-------------------------------------------------------------")
            print("")
            print(oneway.test({wildcards.alpha} ~ formations, data = DATA))  # One-way ANOVA test with no assumption of equal variances
            print("")
            print("")
            print("_______________________________________________________________")
            print("### PAIRWISE t TEST (without assumption of equal variances) ###")
            print("---------------------------------------------------------------")
            print("")
            print(pairwise.t.test(DATA${wildcards.alpha}, DATA$formations, p.adjust.method = "BH", pool.sd = FALSE)) # Pairwise t-tests with no assumption of equal variances
            #
            ### DISTRIBUTION NORMALITY ??? Shapiro-Wilk test --> H0 : residuals / samples follow normal law
            {wildcards.ext}("{output.Normality}")           # Save plot save as .pdf and .png
            plot(RES.AOV, 2)                                # Plot residual
            dev.off()                                       # dev.off
            aov_residuals <- residuals(object = RES.AOV)    # Extract the residuals
            print("")
            print("")
            print("________________________________________")
            print("### SHAPIRO TEST (residue normality) ###")
            print("----------------------------------------")
            print("")
            print(shapiro.test(x = aov_residuals))  # Run Shapiro-Wilk test on residue
            #
            ### ONE-WAY ANOVA on rank (non-parametric) --> H0 : sample follow same distribution
            print("")
            print("")
            print("______________________________________________")
            print("### KRUSKAL-WALLIS H TEST (non-parametric) ###")
            print("----------------------------------------------")
            print("")
            print(kruskal.test({wildcards.alpha} ~ formations, data = DATA)) # Kruskal-Wallis H test
            #
            ### POST-HOC (non-parametric) TESTS / Multiple Comparison (non-parametric) Test
            print("")
            print("")
            print("____________________________")
            print("### DUNN TEST (post-hoc) ###")
            print("----------------------------")
            print("")
            DUNN<-dunn.test(DATA${wildcards.alpha}, DATA$formations, method = "bh", kw = TRUE, alpha = 0.05, altp = TRUE) # Dunn's test
            #
            VEC <- c(DUNN$altP.adjusted) # p.val vec
            names(VEC) <- c(paste((sapply(strsplit(as.character(DUNN$comparisons), " - "), "[[", 1)), (sapply(strsplit(as.character(DUNN$comparisons), " - "), "[[", 2)), sep="-"))
            vecL <- multcompLetters(VEC)
            print(vecL)
            print("")
            print("")
            print("_________________________________________")
            print("### PAIRWISE WILCOXON TEST (post-hoc) ###")
            print("-----------------------------------------")
            print("")
            print(pairwise.wilcox.test(DATA${wildcards.alpha}, DATA$formations, p.adjust.method = "BH"))                   # Pairewise Wilcoxon test with corrections (BH) for multiple testing
            #
            ### PERM-ANOVA
            RES.AOVP <- aovp({wildcards.alpha} ~ formations, data = DATA, perm="Exact", seqs=TRUE, center=TRUE, projections = TRUE, qr = TRUE)
            print("")
            print("")
            print("___________________________________")
            print("### PERM-ANOVA (non-parametric) ###")
            print("-----------------------------------")
            print("")
            print(summary(RES.AOVP))
            #
            ### CLOSE SINK
            sink()
            #
            ### BARPLOT_SAMPLE
            ggplot(data = DATA, aes(x = samples, y = {wildcards.alpha})) +            # ggplot
            ggtitle("{wildcards.data} {wildcards.primer} {wildcards.alpha}") +        # title
            geom_bar(stat = "identity", aes(fill = formations)) +                     # geom_bar
            #scale_fill_manual(values = c('col1', 'col2', 'col3', 'col4', 'col5')) +   # color set manual
            scale_fill_manual(values = c({params.color})) +                           # color set configfile
            #scale_fill_brewer(palette = "Set1") +                                     # color set 1
            theme(axis.text.x = element_text(angle = 90, hjust = 1))                  # theme
            ggsave("{output.BarplotS}")                                               # save as .pdf and .png
            #
            ### BARPLOT_FORMATION
            ggplot(data = FORM, aes(x = formations, y = mean)) +                             # ggplot
            ggtitle("{wildcards.data} {wildcards.primer} {wildcards.alpha}") +               # title
            geom_bar(stat = "identity", aes(fill = formations)) +                            # geom_bar
            geom_errorbar(aes(ymax = mean + sd, ymin = mean - sd), size = .5, width = .5) +  # errorbar
            #scale_fill_manual(values = c('col1', 'col2', 'col3', 'col4', 'col5')) +          # color set manual
            scale_fill_manual(values = c({params.color})) +                                  # color set configfile
            #scale_fill_brewer(palette = "Set1") +                                            # color set 1
            theme(axis.text.x = element_text(angle = 90, hjust = 1))                         # theme
            ggsave("{output.BarplotM}")                                                      # save as .pdf and .png
            #
            ### BOXPLOT_FORMATION
            ggplot(data = DATA, aes(x = formations, y = {wildcards.alpha}, fill = formations)) +                  # ggplot
            ggtitle("{wildcards.data} {wildcards.primer} {wildcards.alpha}") +                                    # title
            geom_boxplot() +                                                                                      # geom_boxplot
            #scale_fill_manual(values = c('col1', 'col2', 'col3', 'col4', 'col5')) +                               # color set manual
            scale_fill_manual(values = c({params.color})) +                                                       # color set configfile
            #scale_fill_brewer(palette = "Set1") +                                                                 # color set 1
            stat_summary(fun.y = mean, geom = "point",colour = "darkred", size = 2.5) +                           # Mean dot
            geom_text(data = FORM, aes(label = round(mean,2), y = mean), nudge_x = -0.25) +                       # Mean label
            geom_text(data = FORM, aes(label = paste("\u00B1", round(sd,2), sep = ""), y = mean), nudge_x = 0.25) # Mean SD and 'U+00B1' is Unicode for latin "plus minus"
            ggsave("{output.Boxplot}")                                                                            # save as .pdf and .png
        """)

####################
rule Beta_NMDS:
    #Aim: Calcul and draw beta diversity matrix nMDS.
    #Use: "vegan" and "ggplot2" R packages
    log:
        'out/{data}/{primer}/r/nmds/{filt,.*}/{beta}.log'
    params:
        mirror = config['r']['mirror'],
        order = lambda wildcards: get_order(wildcards.data),
        color = lambda wildcards: get_color(wildcards.data),
        shape = lambda wildcards: get_shape(wildcards.data),
        names = lambda wildcards: get_names(wildcards.data)
    input:
        Matrix = 'out/{data}/{primer}/qiime2/export/core/{filt,.*}-distance-{beta}/distance-matrix.tsv'
    output:
        NMDS = 'out/{data}/{primer}/r/nmds/{filt,.*}/{beta}.{ext}'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### LOG
            sink("{log}") # Open sink log
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("ggplot2")) {{install.packages("ggplot2", repos='{params.mirror}')}}
            if (!require("ggthemes")) {{install.packages("ggthemes", repos='{params.mirror}')}}
            if (!require("RColorBrewer")) {{install.packages("RColorBrewer", repos='{params.mirror}')}}
            if (!require("gridExtra")) {{install.packages("gridExtra", repos='{params.mirror}')}}
            if (!require("vegan")) {{install.packages("vegan", repos='{params.mirror}')}}
            #
            library(ggplot2)      # You provide the data, tell 'ggplot2' how to map variables to aesthetics, what graphical primitives to use, and it takes care of the details.
            library(ggthemes)     # This package contains extra themes, scales, and geoms, and functions for and related to 'ggplot2'.
            library(RColorBrewer) # Creates nice looking color palettes especially for thematic maps. Use: display.brewer.all()
            library(grid)         # 'grid' adds an 'nx' by 'ny' rectangular grid to an existing plot.
            library(gridExtra)    # Provides a number of user-level functions to work with 'grid' graphics, notably to arrange multiple grid-based plots on a page, and draw tables.
            library(vegan)        # The 'vegan' package provides tools for descriptive community ecology. It has most basic functions of diversity analysis, community ordination and dissimilarity analysis.
            #
            DISTMATRIX<-read.delim("{input.Matrix}", dec=".", head=T) # Import matrix as table
            rownames(DISTMATRIX)<-DISTMATRIX[,1]                      # Fix rownames as column 1
            DISTMATRIX<-DISTMATRIX[,-1]                               # Del column 1
            #
            set.seed(28275716)                                                                                # This will set the seed so that the random draw will be the same
            DISTMATRIX.MDS <- metaMDS(DISTMATRIX)                                                             # Using all the defaults
            DISTMATRIX.SCORE <- as.data.frame(scores(DISTMATRIX.MDS))                                         # Using the SCORE function from vegan to extract the site SCORE and convert to a data.frame
            DISTMATRIX.SCORE$formation <- sapply(strsplit(as.character(row.names(DISTMATRIX)), "_"), "[[", 1) # Create a column of formation names, from the rownames of DISTMATRIX
            DISTMATRIX.SCORE$formation <- as.factor(DISTMATRIX.SCORE$formation)                               # formations as factor
            DISTMATRIX.SCORE$formation <- ordered(DISTMATRIX.SCORE$formation, levels = c({params.order}))     # re-order levels (mod) in the wanted order
            DISTMATRIX.SCORE$plot <- sapply(strsplit(as.character(row.names(DISTMATRIX)), "_"), "[[", 2)      # Create a column of plot names, from the rownames of DISTMATRIX
            #
            ### GGPLOT
            ggplot(data=DISTMATRIX.SCORE, aes(x=NMDS1, y=NMDS2, colour=formation, shape=formation)) +     # ggplot (label=DISTMATRIX.SCORE$plot)
            scale_colour_manual(labels=c({params.names}), values=c({params.color})) +                     # color + names set bioindic
            scale_shape_manual(labels=c({params.names}), values=c({params.shape})) +                      # shape + names set bioindic
            geom_point(size=2) +                                                                          # geom size
            #geom_text(size=4, check_overlap=F, vjust=0, hjust=0, angle=45, nudge_x=0.01, nudge_y=0.01) +  # geom text
            coord_equal() +                                                                               # coord
            #stat_ellipse(level=0.95, type="t", geom="polygon", alpha=0.5, aes(fill=formation)) +          # confidance ellipses
            #geom_polygon(alpha=0.5) +                                                                     # area polygons
            geom_density2d(alpha=0.5, linetype="solid", size=0.1) +                                       # density curves
            expand_limits(x = c(1.3*min(DISTMATRIX.SCORE$NMDS1),                                          # expand limit inf. x
                                1.3*max(DISTMATRIX.SCORE$NMDS1)),                                         # expand limit sup. y
                          y = c(1.3*min(DISTMATRIX.SCORE$NMDS2),                                          # expand limit inf. x
                                1.3*max(DISTMATRIX.SCORE$NMDS2))) +                                       # expand limit sup. y
            #ggtitle("nMDS on {wildcards.data} {wildcards.primer} {wildcards.beta} distance matrix") +     # title
            theme_bw() +                                                                                  # theme (Thnaks to Christopher Chizinski, Asst. Professor of Human Dimensions of Wildlife)
            theme(axis.text.x = element_blank(),                                                          # remove x-axis text
                  axis.text.y = element_blank(),                                                          # remove y-axis text
                  axis.ticks = element_blank(),                                                           # remove axis ticks
                  axis.title.x = element_blank(),                                                         # remove x-axis labels
                  axis.title.y = element_blank(),                                                         # remove y-axis labels
                  panel.background = element_blank(),                                                     # remove background
                  panel.grid.major = element_blank(),                                                     # remove major-grid labels
                  panel.grid.minor = element_blank(),                                                     # remove minor-grid labels
                  plot.background = element_blank())                                                      # remove background
            ggsave("{output.NMDS}")                                                                       # save as .pdf and .png
            sink()
        """)

####################
rule Abund_PCA:
    #Aim: Calcul and draw Abundance PCA.
    #Use: "vegan" and "ggplot2" R packages
    log:
        'out/{data}/{primer}/r/pca/{filt,.*}/abundance.log'
    params:
        mirror = config['r']['mirror'],
        color = lambda wildcards: get_color(wildcards.data)
    input:
        Abundance = 'out/{data}/{primer}/qiime2/export/core/{filt,.*}Table/ESV.tsv'
    output:
        PCA = 'out/{data}/{primer}/r/pca/{filt,.*}/Abundance.{ext}'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### LOG
            sink("{log}") # Open sink log
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("ggplot2")) {{install.packages("ggplot2", repos='{params.mirror}')}}
            if (!require("ggthemes")) {{install.packages("ggthemes", repos='{params.mirror}')}}
            if (!require("RColorBrewer")) {{install.packages("RColorBrewer", repos='{params.mirror}')}}
            if (!require("gridExtra")) {{install.packages("gridExtra", repos='{params.mirror}')}}
            if (!require("vegan")) {{install.packages("vegan", repos='{params.mirror}')}}
            #
            ### LOAD LIBRARIES
            library(ggplot2)      # You provide the data, tell 'ggplot2' how to map variables to aesthetics, what graphical primitives to use, and it takes care of the details.
            library(ggthemes)     # This package contains extra themes, scales, and geoms, and functions for and related to 'ggplot2'.
            library(RColorBrewer) # Creates nice looking color palettes especially for thematic maps. Use: display.brewer.all()
            library(grid)         # 'grid' adds an 'nx' by 'ny' rectangular grid to an existing plot.
            library(gridExtra)    # Provides a number of user-level functions to work with 'grid' graphics, notably to arrange multiple grid-based plots on a page, and draw tables.
            library(vegan)        # The 'vegan' package provides tools for descriptive community ecology. It has most basic functions of diversity analysis, community ordination and dissimilarity analysis.
            #
            ABUND<-read.delim("{input.Abundance}", dec=".", head=T) # Import matrix as table
            ESV<-ABUND$ESV                                          # remember the names (ESV)
            ABUND<-as.data.frame(t(ABUND[,-1]))                     # transpose all but the first column (ESV)
            colnames(ABUND)<-ESV                                    # rename column with names (ESV)
            #
            ABUND.PCA<-prcomp(ABUND)                                                            # Using all the defaults
            ABUND.OUT<-as.data.frame(ABUND.PCA$x)                                               # Using the OUT function from vegan to extract the site OUT and convert to a data.frame
            ABUND.OUT$formation<-sapply(strsplit(as.character(row.names(ABUND)), "_"), "[[", 1) # Create a column of formation names, from the rownames of DISTMATRIX
            ABUND.OUT$plot<-sapply(strsplit(as.character(row.names(ABUND)), "_"), "[[", 2)      # Create a column of plot names, from the rownames of DISTMATRIX
            #
            percentage<-round(ABUND.PCA$sdev / sum(ABUND.PCA$sdev) * 100, 2)                                 # Calcul percentage axis
            percentage<-paste(colnames(ABUND.OUT), "(", paste( as.character(percentage), "%", ")", sep="") ) # Print percentage
            #
            ### GGPLOT
            ggplot(ABUND.OUT, aes(x=PC1, y=PC2, color=formation, label=ABUND.OUT$plot)) +                # ggplot
            #scale_fill_manual(values = c('col1', 'col2', 'col3', 'col4', 'col5')) +                      # color set manual
            scale_colour_manual(values=c({params.color})) +                                              # color set configfile
            #scale_fill_brewer(palette="Set1") +                                                          # color set 1
            geom_point(size=3) +                                                                         # geom size
            geom_text(size=4, check_overlap=F, vjust=0, hjust=0, angle=45, nudge_x=0.01, nudge_y=0.01) + # geom text
            ggtitle("PCA on {wildcards.primer} ESVs count") +                                            # title
            xlab(percentage[1]) +                                                                        # x label
            ylab(percentage[2]) +                                                                        # y label
            theme_bw() +                                                                                 # theme
            stat_ellipse(level=0.95, type="t", geom="polygon", alpha=0.1, aes(fill = formation))         # confidance ellipse
            ggsave("{output.PCA}")                                                                       # save as .pdf and .png
            sink()
        """)

####################
rule Frequency_Species:
    #Aim: Calcul and draw frequency-species graph.
    #Use: "vegan" R package
    log:
        'out/{data}/{primer}/r/frequency/{filt,.*}/frequency.log'
    params:
        mirror = config['r']['mirror']
    input:
        Abundance = 'out/{data}/{primer}/qiime2/export/core/{filt,.*}Table/ESV.tsv'
    output:
        Frequency = 'out/{data}/{primer}/r/frequency/{filt,.*}/frequency-species.{ext}'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### LOG
            sink("{log}") # Open sink log
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("vegan")) {{install.packages("vegan", repos='{params.mirror}')}}
            #
            ### LOAD LIBRARIES
            library(vegan)        # The 'vegan' package provides tools for descriptive community ecology. It has most basic functions of diversity analysis, community ordination and dissimilarity analysis.
            #
            ABUND<-read.delim("{input.Abundance}", dec=".", head=T) # Import abundance as table
            ESV<-ABUND$ESV                      # remember the names (ESV)
            ABUND<-as.data.frame(t(ABUND[,-1])) # transpose all but the first column (ESV)
            colnames(ABUND)<-ESV                # rename column with names (ESV)
            #
            fish<-fisherfit(ABUND)                                                # calcul fisher fit on abund data
            {wildcards.ext}("{output.Frequency}")                                 # save as .pdf and .png
            plot(fish)                                                            # plot fish
            title(main = "{wildcards.primer} {wildcards.data} frequency-species") # title
            dev.off()                                                             # dev off
            sink()
        """)

####################
rule Rank_Abundances:
    #Aim: Calcul and draw rank-abundances graph.
    #Use: "vegan" R package
    log:
        'out/{data}/{primer}/r/rank/{filt,.*}/rank.log'
    params:
        mirror = config['r']['mirror']
    input:
        Abundance = 'out/{data}/{primer}/qiime2/export/core/{filt,.*}Table/ESV.tsv'
    output:
        Done = 'out/{data}/{primer}/r/rank/{filt,.*}/rank-abundance.done'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### LOG
            sink("{log}") # Open sink log
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("vegan")) {{install.packages("vegan", repos="{params.mirror}")}}
            #
            ### LOAD LIBRARIES
            library(vegan)        # The 'vegan' package provides tools for descriptive community ecology. It has most basic functions of diversity analysis, community ordination and dissimilarity analysis.
            #
            ABUND<-read.delim("{input.Abundance}", dec=".", head=T) # Import abundance as table
            ESV<-ABUND$ESV                                          # remember the names (ESV)
            ABUND<-as.data.frame(t(ABUND[,-1]))                     # transpose all but the first column (ESV)
            colnames(ABUND)<-ESV                                    # rename column with names (ESV)
            #
            for (sample in rownames(ABUND)) {{                                                                                            # for loop in each sample
                    rad<-radfit(ABUND[sample,])                                                                                            # calcul fisher fit on abund data
                    pdf(paste("out/{wildcards.data}/{wildcards.primer}/r/rank/{wildcards.filt}/", sample, "-rank-abundances.pdf", sep="")) # save as pdf
                    plot(rad)                                                                                                              # plot rad
                    title(main = paste(sample, "rank-abundances", sep=" "))                                                                # title
                    dev.off()                                                                                                              # dev off
                    png(paste("out/{wildcards.data}/{wildcards.primer}/r/rank/{wildcards.filt}/", sample, "-rank-abundances.png", sep="")) # save as png
                    plot(rad)                                                                                                             # plot rad
                    title(main = paste("{wildcards.data} {wildcards.primer}", sample, "rank-abundances", sep=" "))                        # title
                    dev.off()}}                                                                                                           # dev off

            sink()
            sink("{output.Done}") # Output.Done
            sink()
        """)

####################
rule Adonis:
    #Aim: Perform PermANOVA.
    #Use: "vegan" R package with adonis
    log:
        'out/{data}/{primer}/r/adonis/adonis.log'
    params:
        mirror = config['r']['mirror'],
        metadata = config['datasets']['metadata'],
        permutations = config['permanova']['permutations']
    input:
        Abundance = 'out/{data}/{primer}/qiime2/export/core/{filt,.*}Table/ESV.tsv'
    output:
        Adonis = 'out/{data}/{primer}/r/adonis/{filt,.*}/adonis-{category}-PermANOVA-{methode}.txt'
    run:
        R("""
            ### REMOVE PUTATIVE OLD ENVIRONMENT
            rm(list=ls())
            #
            ### LOG
            sink("{log}") # Open sink log
            #
            ### INSTALL PACKAGES IF REQUIRE
            if (!require("vegan")) {{install.packages("vegan", repos="{params.mirror}")}}
            library(devtools)
            install_github("pmartinezarbizu/pairwiseAdonis/pairwiseAdonis")
            #
            ### LOAD LIBRARIES
            library(vegan)          # The 'vegan' package provides tools for descriptive community ecology. It has most basic functions of diversity analysis, community ordination and dissimilarity analysis.
            library(pairwiseAdonis) # This is an R wrapper function for multilevel pairwise comparison using adonis (~Permanova) from package 'vegan'.
            #
            ABUND<-read.delim("{input.Abundance}", dec=".", head=T) # Import abundance as table
            ESV<-ABUND$ESV                                          # Remember the names (ESV)
            ABUND<-as.data.frame(t(ABUND[,-1]))                     # Transpose all but the first column (ESV)
            colnames(ABUND)<-ESV                                    # Rename column with names (ESV)
            #
            ABUND.ENV<-read.delim("{params.metadata}", dec=".", head=T)    # Import metadata as table
            colnames(ABUND.ENV)[1]<-"sampleID"                             # Rename column 1 as "sampleID"
            ABUND.ENV<-ABUND.ENV[ABUND.ENV$sampleID %in% rownames(ABUND),] # Filter metadata "sampleID" by samples in Abundance
            #
            sink()
            sink("{output.Adonis}")
            print(adonis(ABUND ~ {wildcards.category},
                         data = ABUND.ENV,
                         permutations = {params.permutations},
                         method = "{wildcards.methode}"))
            #
            ABUND${wildcards.category}<-ABUND.ENV${wildcards.category}
            print(pairwise.adonis(ABUND[,1:length(ABUND[1,])-1],
                                  ABUND${wildcards.category},
                                  sim.function = "vegdist",
                                  sim.method = "{wildcards.methode}",
                                  p.adjust.m = "BH",
                                  reduce = NULL,
                                  perm = {params.permutations}))
            sink()
        """)
