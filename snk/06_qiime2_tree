###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: construct a rooted phylogenetic tree
# Date: 2017.11.02
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.14
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

# Params
THREADS = config["cluster"]["threads"] # general

###############################################################################
rule all:
    input:
       rooted_tree  = expand("out/{project}/{primers}/qiime2/tree/"
                             +"RootedTree.qza",
                             project = PROJECT,
                             primers = PRIMERS)

###############################################################################
rule midpoint_root:
    # Aim: Midpoint root an unrooted phylogenetic tree
    # Use: qiime phylogeny midpoint-root [OPTIONS]
    conda:
        QIIME2
    input:
        unrooted_tree = "out/{project}/{primers}/qiime2/tree/UnrootedTree.qza"
    output:
        rooted_tree = "out/{project}/{primers}/qiime2/tree/RootedTree.qza"
    shell:
        "qiime phylogeny midpoint-root "
        "--i-tree {input.unrooted_tree} "
        "--o-rooted-tree {output.rooted_tree}"

###############################################################################
rule fast_tree:
    # Aim: Construct an unrooted phylogenetic tree with FastTree
    # Use: qiime phylogeny fasttree [OPTIONS]
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        masked_repseq = "out/{project}/{primers}/qiime2/tree/MaskRepSeq.qza"
    output:
        unrooted_tree = "out/{project}/{primers}/qiime2/tree/UnrootedTree.qza"
    shell:
        "qiime phylogeny fasttree "
        "--i-alignment {input.masked_repseq} "
        "--p-n-threads {params.threads} "
        "--o-tree {output.unrooted_tree}"

###############################################################################
rule mask:
    # Aim: Mask unconserved and highly gapped columns from an alignment
    # Use: qiime alignment mask [OPTIONS]
    conda:
        QIIME2
    input:
        aligned_repseq = "out/{project}/{primers}/qiime2/tree/AlignRepSeq.qza"
    output:
        masked_repseq = "out/{project}/{primers}/qiime2/tree/MaskRepSeq.qza"
    shell:  
        "qiime alignment mask "
        "--i-alignment {input.aligned_repseq} "
        "--o-masked-alignment {output.masked_repseq}"

###############################################################################
rule mafft:
    # Aim: Perform de novo multiple sequence alignment using MAFFT
    # Use: qiime alignment mafft [OPTIONS]
    conda:
        QIIME2
    params:
        threads = THREADS
    input:
        filtered_repseq = "out/{project}/{primers}/qiime2/core/ConRepSeq.qza"
    output:
        aligned_repseq = "out/{project}/{primers}/qiime2/tree/AlignRepSeq.qza"
    shell:  
        "qiime alignment mafft "
        "--i-sequences {input.filtered_repseq} "
        "--p-n-threads {params.threads} "
        "--o-alignment {output.aligned_repseq}"

###############################################################################
